#!/usr/bin/python
#
################################################################################
# Imports
import MySQLdb as mdb
import time
import os

################################################################################
# define some constants
dbServer = "localhost"
dbUser = "piradmin"
dbPass = "piradmin"
dbDatabase = "piraddb"
dbTable = "sources"

################################################################################
# class for rotary encoder
class PiradDB:
  
  con = None
  initialised = False

  ##############################################################################
  # initialise state
  def __init__(self):
    self.dbInit()

  ##############################################################################
  # intialise database connection
  def dbInit(self):

    try:

      self.con = mdb.connect(dbServer, dbUser, dbPass, dbDatabase)      
      self.initialised = True
      print "DB initialised"
        
    except mdb.Error, e:
      
      print "Error %d: %s" % (e.args[0],e.args[1])
      sys.exit(1)
        
    finally:              
      if self.con:    
        self.con.close()
        
  
  ##############################################################################
  # determine if connection to DB is possible
  def canConnect(self):
    return self.initialised

  ##############################################################################
  # get station URL list
  def dbGetStations(self, urlIn, niceIn):
    if self.initialised:
      try:
        self.con = mdb.connect(dbServer, dbUser, dbPass, dbDatabase)
        sql = "SELECT url,description FROM sources;"
        
        with self.con: 
          cur = self.con.cursor()
          cur.execute(sql)

          rows = cur.fetchall()

          self.sourceList = []
          for row in rows:
            urlIn.append(row[0])
            niceIn.append(row[1])
#            sourceList.append(row[0])
                  
      except mdb.Error, e:      
        print "Error %d: %s" % (e.args[0],e.args[1])
        self.initialised = False
        sys.exit(1)
          
      finally:              
        if self.con:    
          self.con.close()

#      return sourceList
      return True
      
    else:
      return None      
#      return False
      
  ##############################################################################
  # get setting value
  def dbGetSetting(self, key):
  
    retVal = None
    if self.initialised:
      try:
        self.con = mdb.connect(dbServer, dbUser, dbPass, dbDatabase)
        sql = "SELECT value FROM settings where item = \'" + key + "\';"
        
        
        with self.con: 
          cur = self.con.cursor()
          cur.execute(sql)
        
          retVal = cur.fetchone()
          if retVal <> None:
            retVal = retVal[0]
                  
      except mdb.Error, e:      
        print "Error %d: %s" % (e.args[0],e.args[1])
        self.initialised = False
        sys.exit(1)
          
      finally:              
        if self.con:    
          self.con.close()
     
    return retVal     
      
  ##############################################################################
  # set setting value
  def dbSetSetting(self, key, value):
  
    if self.initialised:
      try:
        self.con = mdb.connect(dbServer, dbUser, dbPass, dbDatabase)
        sql = "UPDATE settings SET value= '" + value + "' WHERE item ='" + key + "';"
        
        try:
          with self.con:
            cur = self.con.cursor()
            cur.execute(sql)
            self.con.commit()
        except:
          print "error committing data"
          self.con.rollback()
        
      except mdb.Error, e:      
        print "Error %d: %s" % (e.args[0],e.args[1])
        self.initialised = False
        sys.exit(1)
          
      finally:              
        if self.con:    
          self.con.close()
     
      
