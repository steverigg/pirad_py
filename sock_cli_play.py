# Echo client program
import socket

HOST = 'localhost'    # The remote host
PORT = 50006              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.sendall('PLAY')
data = s.recv(1024)
s.shutdown(1)
s.close()
print 'Received', repr(data)
