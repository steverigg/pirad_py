#!/usr/bin/python
#
################################################################################
# Imports
import time
import os

################################################################################
# Define constants 

MENU_STOP       = "STP"
MENU_RELOAD     = "RLD"
MENU_BACK       = "BCK"
MENU_REBOOT     = "RBT"
MENU_HALT       = "HLT"
MENU_RESET      = "RST"
MENU_IPADDR     = "IPA"
MENU_SSID       = "SID"
MENU_MAC        = "MAC"


################################################################################
# class for Menu system
class PiradMenu:

  topMenu = ["Select a stream", 
              "Stop playing", 
              "Reload streams",
              "System",
              "Back"] # main LCD menu

  sysMenu = ["Network",
              "Reboot",
              "Shutdown",
              "Back"] # system menu
              
  netMenu = ["IP address",
              "SSID",
              "MAC address",
              "Back"] # network menu

  streamList = []   # holds the stream urls
  musicList = []    # holds local music track list
  
  menuIdx = 0       # menu index   
  menuTxt = ""      # menu text
  
  currentMenu = topMenu
  
  displayed = False

  ##############################################################################
  # initialise state
  def __init__(self):
    streamList = []
    
  ##############################################################################
  # load all streams
  def loadStreamList(self, stream):
    self.streamList = stream
  
  ##############################################################################
  # add single stream
  def addStream(self, stream):
    self.streamList.append(stream)
  
  ##############################################################################
  # clear streams
  def clearStream(self):
    self.streamList = []
  
  ##############################################################################
  # next item selected
  def nextItem(self):
    if(self.menuDisplayed()):
      if (self.menuIdx < len(self.currentMenu) - 1):
        self.menuIdx += 1
      return self.currentMenu[self.menuIdx]

  ##############################################################################
  # prev item selected
  def prevItem(self):
    if(self.menuDisplayed()):
      if (self.menuIdx > 0):
        self.menuIdx -= 1
      return self.currentMenu[self.menuIdx]

  ##############################################################################
  # reset menu status (top level)
  def reset(self):
    self.menuIdx = 0
    self.currentMenu = self.topMenu
    self.displayed = False


  ##############################################################################
  # select button pressed
  def select(self):
    self.retVal = ""
    
    if (not self.menuDisplayed()):
      print "menu requested"
      self.displayed = True
      self.menuIdx = 0
      self.retVal = self.currentMenu[self.menuIdx]
    else:
      if (self.currentMenu == self.topMenu):
        if (self.menuIdx == 1):
          print "stopping"
          self.retVal =  MENU_STOP
          self.displayed = False
        elif (self.menuIdx == 2):
          print "reloading"
          self.retVal = MENU_RELOAD
          self.displayed = False
        elif (self.menuIdx == 3):
          print "system menu"
          self.menuIdx = 0
          self.currentMenu = self.sysMenu
          self.retVal = self.currentMenu[self.menuIdx]
        elif (self.menuIdx == 4):
          print "doing nothing"
          self.menuIdx = 0
          self.retVal = MENU_BACK
          self.displayed = False
        elif (self.menuIdx == 5):
          print "resetting"
          self.retVal = MENU_RESET
          self.displayed = False
        else:
          print "stream selection"
          self.currentMenu = self.streamList
          self.menuIdx = 0
          self.retVal = self.currentMenu[self.menuIdx]          

      # system menu
      elif (self.currentMenu == self.sysMenu):
        if(self.menuIdx == 0):
          self.menuIdx = 0
          self.currentMenu = self.netMenu
          self.retVal = self.currentMenu[self.menuIdx]          
        elif(self.menuIdx == 1):
          print "rebooting"
          self.retVal = MENU_REBOOT
          self.displayed = False        
        elif(self.menuIdx == 2):
          print "shutting down"
          self.retVal = MENU_HALT
          self.displayed = False        
        else:
          self.currentMenu = self.topMenu
          self.menuIdx = 0
          self.retVal = self.currentMenu[self.menuIdx]

      # network menu
      elif (self.currentMenu == self.netMenu):
        if(self.menuIdx == 0):
          print "show IP addr"
          self.displayed = False        
          self.menuIdx = 0
          self.currentMenu = self.topMenu
          self.retVal = MENU_IPADDR
        elif(self.menuIdx == 1):
          print "show SSID"
          self.displayed = False        
          self.menuIdx = 0
          self.currentMenu = self.topMenu
          self.retVal = MENU_SSID
        elif(self.menuIdx == 2):
          print "show MAC"
          self.displayed = False        
          self.menuIdx = 0
          self.currentMenu = self.topMenu
          self.retVal = MENU_MAC
        else:
          self.currentMenu = self.sysMenu
          self.menuIdx = 0
          self.retVal = self.currentMenu[self.menuIdx]

      # selecting a station
      else:
        self.currentMenu = self.topMenu
        self.retVal = self.menuIdx
        self.displayed = False
        self.menuIdx = 0
      
    return self.retVal

    ##############################################################################
  # return whether menu is being displayed
  def menuDisplayed(self):
    return self.displayed
    

