#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports
import socket
import SocketServer
import threading
import time

import Globals

# constants
HOST = ''        # Symbolic name meaning all available interfaces
PORT = 50006              # Arbitrary non-privileged port


################################################################################
# classes for the web interface

################################################################################
# class to handle the received network messages
class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
  piradmin = None
  def handle(self):
    data = self.request.recv(1024)        
#    print "Data: " + data
    if (data == "STOP"):
      self.piradmin.stop()
      self.request.sendall("STOPPED")
    elif (data == "PLAY"):
      self.piradmin.play()
      self.request.sendall("PLAYING")
    elif (data == "INFO"):
      info = self.piradmin.getStreamInfo()
      self.request.sendall(info)
    elif (data[:3] == "SEL"):
#      print data
      chunks = data.split()
      self.piradmin.play(chunks[1])
      self.request.sendall("PLAYING")
    elif (data == "LIST"):
      streamNames = []
      streamUrls = []
      if (self.piradmin.getStreamListFromDB(streamUrls, streamNames)):
        streamList = ""
        for i in range(len(streamNames)):
          streamList += streamNames[i] + ";"
        self.request.sendall(streamList)
    elif (data == "TEST"):
      time.sleep(0.01)
      self.request.sendall("OK")
    elif (data == "LOAD"):
      self.piradmin.loadStreams()
      self.request.sendall("RELOADED")
        
      
      

################################################################################
# factory class for handling the thread
class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
  pass

################################################################################
# server manager class
#  handles threading of the web interface
#  takes the pirad admin class to send messages to
class ServerManager():
  def __init__(self, anAdminObject):
    class ThreadedTCPRequestHandlerWithAdmin(ThreadedTCPRequestHandler):
      piradmin = anAdminObject
    self.server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandlerWithAdmin)
    
  def start(self):             
    server_thread = threading.Thread(target=self.server.serve_forever)
    server_thread.start()

  def stop(self):
    self.server.shutdown()    
