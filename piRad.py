#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports

# system imports
import RPi.GPIO as GPIO
import time 
import os

# local imports
import PiradDB
import LCD
import PiradMenu
import RotaryEncode
import Switch
import PiradAdmin
import WebInterface
import Globals

################################################################################
# constants
encPinA = 4
encPinB = 5
swPin = 6

LONGDELAY = 20

# to reduce noise on the encoder wheel, several "hits" in one direction
# will be considered befoer actually indexing
ENC_TRIGGER = 3

################################################################################
# Globals  (object instantiation)
lcdDisplay = LCD.LCD_16x2()
db = PiradDB.PiradDB()
menu = PiradMenu.PiradMenu()
admin = PiradAdmin.PiradAdmin(lcdDisplay, db, menu)
rotEncode = RotaryEncode.RotaryEncoder.Worker(encPinA, encPinB)
pushButton = Switch.Switch(swPin)

debug = False

################################################################################
# initialise
def init():

  # init rotary encoder
  rotEncode.start()
  admin.loadStreams()
  lcdDisplay.lcdSetBackLight(LCD.BKLT_ON)

################################################################################
# Main program block
def main():

  admin.state.setMenuState(False)      # menu being browsed?
  admin.state.setStoppedState(False)   # radio is stopped
  lastState = False                    # check state of switch
  stationChanged = True                # new station has been selected
  prevText = ""                        # previous display string
  longPressCount = 0                   # counter for long button press
  encCounter = 0                       # counter for "hits" on the rotary switch
  prevEncDir = True                    # track previous encoder direction

  init()      # initialise program
  admin.lcdTitle()  # display startup text

  scroll = LCD.LCD_16x2.Scroller(lcdDisplay)  # create new scroller
  webIF = WebInterface.ServerManager(admin)
  webIF.start()

  last = admin.getLastStation() # see if a last playing station available
  if last <> None: 
    os.system("mpc play " + str(last))   # start the last played station
  else:
    os.system("mpc play")  # start the first station
    
  # main program loop
  while True:

    # read and display currently playing stream
    if(not admin.state.getMenuState() and not admin.state.getStoppedState()):   # only show stream info if not in menu

      station = admin.getStreamInfo()
      
      # if information has changed, update text
      if (station != prevText):
        scroll.stop()
        if debug: print "new text"
        if debug: print "new station: " + station
        if debug: print "previous station " + prevText
        admin.resetLCD()    # hack for screen corruption :s
        prevText = station
        if len(station) > LCD.LCD_WIDTH:
          scroll = LCD.LCD_16x2.Scroller(lcdDisplay)
          if debug: print "scrolling text updated - " + station
          scroll.updateText(LCD.LCD_LINE_2, station)
          scroll.start()
        else:
          if debug: print "fixed text updated - " + station
          lcdDisplay.lcdText(LCD.LCD_LINE_2, station, 2)
        

    # check for inputs
    
    # button pressed
    swState = pushButton.get_state() 
    if ( swState != lastState ):     # check not in same as previous state
      lastState = swState
      longPressCount = 0
      if(swState == True):
        scroll.stop()
        lcdDisplay.lcdSetBackLight(LCD.BKLT_ON)
        
        if admin.state.getStoppedState(): # if we're stopped, just play
          os.system("mpc play")
          admin.state.setStoppedState(False)
          prevText = ""

        else: # otherwise, open the menu
          admin.state.setMenuState(True)  # start the menu
          retVal = menu.select()          # get current menu entry
          if debug: print "button " + str(retVal)

          # main menu items
          if(Globals.is_number(retVal)):      # check if we received a channel number
            admin.play(retVal + 1)
            
          elif (retVal == PiradMenu.MENU_STOP):    # check if 'stop' was selected
            admin.stop()

          elif (retVal == PiradMenu.MENU_RELOAD):  # check if 'reload' was selected
            admin.reload()

          # system menu items
          elif (retVal == PiradMenu.MENU_REBOOT):  # check if 'reboot' was selected
            admin.reboot()

          elif (retVal == PiradMenu.MENU_HALT):    # check if 'halt' was selected
            admin.shutdown()

          elif (retVal == PiradMenu.MENU_RESET):  # check if 'reset' was selected
            os.system("/home/pi/pirad/killpiRad.sh&")     # and reset piRad if so

          elif (retVal == PiradMenu.MENU_BACK):    # check if 'back' was selected
            admin.state.setMenuState(False)  # leave the menu
            prevText = ""

          # network menu items
          elif (retVal == PiradMenu.MENU_IPADDR):  # check if 'IP Address' was selected
            ipaddr = "IP address: " + admin.getIpAddress()
            scroll = LCD.LCD_16x2.Scroller(lcdDisplay)
            scroll.updateText(LCD.LCD_LINE_2, ipaddr)
            scroll.start()
            admin.state.setMenuState(False)  # leave the menu

          elif (retVal == PiradMenu.MENU_SSID):  # check if 'SSID' was selected
            ssid = "BSSID: " + admin.getEssid()
            scroll = LCD.LCD_16x2.Scroller(lcdDisplay)
            scroll.updateText(LCD.LCD_LINE_2, ssid)
            scroll.start()
            admin.state.setMenuState(False)  # leave the menu

          elif (retVal == PiradMenu.MENU_MAC):  # check if 'MAC address' was selected
            mac = "MAC address: " + admin.getMACAddress()
            scroll = LCD.LCD_16x2.Scroller(lcdDisplay)
            scroll.updateText(LCD.LCD_LINE_2, mac)
            scroll.start()
            admin.state.setMenuState(False)  # leave the menu

          # new menu display
          else:                       # otherwise display next menu item
            lcdDisplay.lcdText(LCD.LCD_LINE_2, retVal, 2)

    else: # long press
      if(swState == True):
        longPressCount += 1           # loop counter for long press
        if longPressCount == LONGDELAY:
          longPressCount = 0          # reset loop counter
          admin.state.setMenuState(False)  # leave the menu
          prevText = ""
          menu.reset()
          admin.resetLCD()




    # rotary encoder event
    delta = rotEncode.get_delta()
    if ( delta != 0 ):    # check if rotating


      cw = delta > 0      # clockwise rotation

      if (cw == prevEncDir):
        encCounter = encCounter + 1 

        if encCounter == ENC_TRIGGER:
          encCounter = 0
          if (cw):
            text = menu.nextItem()  # next menu item
          else:
            text = menu.prevItem()  # prev menu item
      
          if (text != None):
            if debug: print text
            lcdDisplay.lcdText(LCD.LCD_LINE_2, text, 2)
      else:
        prevEncDir = cw


    time.sleep(0.1)

################################################################################
if __name__ == '__main__':
  main()
