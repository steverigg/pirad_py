/* drop database and user */
drop database piraddb;
drop user 'piradmin';

/* create the database */
create database piraddb;

/* create the user and supply priveleges */
create user 'piradmin' identified by 'piradmin';
grant all on piraddb.* to 'piradmin'@'%' identified by 'piradmin';

/* change to the new database */
use piraddb;

/* create the sources and settings tables */
create table sources (id int primary key auto_increment, url varchar(1024) not null, description varchar(256) unique not null);
create table settings (item varchar(32) primary key, value varchar(32));
