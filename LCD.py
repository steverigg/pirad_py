#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports
import RPi.GPIO as GPIO
import time
import os
import threading

import Globals

################################################################################
# Define GPIO mappings

# lcd display
LCD_RS = 2
LCD_E  = 3
LCD_D4 = 4 
LCD_D5 = 17
LCD_D6 = 27
LCD_D7 = 22
LCD_BACKLIGHT = 18

################################################################################
# Define some device constants
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line

BKLT_ON = True
BKLT_OFF = False

# Timing constants
E_PULSE = 0.0001
E_DELAY = 0.0001
INITWAIT = 0.01


################################################################################
# Globals

################################################################################
# class for LCD display panel (16x2)
class LCD_16x2:

  ##############################################################################
  # initialise state
  def __init__(self):
    self.__gpioInit()
    self.__lcdInit()

  ##############################################################################
  # Initialise display
  def __lcdInit(self):

    self.__lcdByte(0x33,LCD_CMD)  
    time.sleep(INITWAIT)
    self.__lcdByte(0x32,LCD_CMD)  
    time.sleep(INITWAIT)
    self.__lcdByte(0x28,LCD_CMD)  
    time.sleep(INITWAIT)
    self.__lcdByte(0x0C,LCD_CMD)  
    time.sleep(INITWAIT)
    self.__lcdByte(0x06,LCD_CMD)  
    time.sleep(INITWAIT)
    self.__lcdByte(0x01,LCD_CMD)  
    time.sleep(INITWAIT)

  ##############################################################################
  # intialise GPIO interfaces
  def __gpioInit(self):

    GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
    GPIO.setwarnings(False)
       
    # outputs for lcd
    GPIO.setup(LCD_E, GPIO.OUT)  # E
    GPIO.setup(LCD_RS, GPIO.OUT) # RS
    GPIO.setup(LCD_D4, GPIO.OUT) # DB4
    GPIO.setup(LCD_D5, GPIO.OUT) # DB5
    GPIO.setup(LCD_D6, GPIO.OUT) # DB6
    GPIO.setup(LCD_D7, GPIO.OUT) # DB7

    GPIO.setup(LCD_BACKLIGHT, GPIO.OUT) # Back light
    

  ##############################################################################
  # Send string to display
  # style=1 Left justified
  # style=2 Centred
  # style=3 Right justified
  def __lcdString(self,message,style):

    if style==1:
      message = message.ljust(LCD_WIDTH," ")  
    elif style==2:
      message = message.center(LCD_WIDTH," ")
    elif style==3:
      message = message.rjust(LCD_WIDTH," ")

    for i in range(LCD_WIDTH):
      self.__lcdByte(ord(message[i]),LCD_CHR)

  ##############################################################################
  # Send byte to data pins
  # bits = data
  # mode = True  for character
  #        False for command
  def __lcdByte(self,bits, mode):

    GPIO.output(LCD_RS, mode) # RS
    # High bits
    GPIO.output(LCD_D4, False)
    GPIO.output(LCD_D5, False)
    GPIO.output(LCD_D6, False)
    GPIO.output(LCD_D7, False)
    time.sleep(E_DELAY)

    if bits&0x10==0x10:
      GPIO.output(LCD_D4, True)
    if bits&0x20==0x20:
      GPIO.output(LCD_D5, True)
    if bits&0x40==0x40:
      GPIO.output(LCD_D6, True)
    if bits&0x80==0x80:
      GPIO.output(LCD_D7, True)
    time.sleep(E_DELAY)

    # Toggle 'Enable' pin
    time.sleep(E_DELAY)    
    GPIO.output(LCD_E, True)  
    time.sleep(E_PULSE)
    GPIO.output(LCD_E, False)  
    time.sleep(E_DELAY)      

    # Low bits
    GPIO.output(LCD_D4, False)
    GPIO.output(LCD_D5, False)
    GPIO.output(LCD_D6, False)
    GPIO.output(LCD_D7, False)
    time.sleep(E_DELAY)      

    if bits&0x01==0x01:
      GPIO.output(LCD_D4, True)
    if bits&0x02==0x02:
      GPIO.output(LCD_D5, True)
    if bits&0x04==0x04:
      GPIO.output(LCD_D6, True)
    if bits&0x08==0x08:
      GPIO.output(LCD_D7, True)
    time.sleep(E_DELAY)

    # Toggle 'Enable' pin
    time.sleep(E_DELAY)    
    GPIO.output(LCD_E, True)  
    time.sleep(E_PULSE)
    GPIO.output(LCD_E, False)  
    time.sleep(E_DELAY)   

    time.sleep(E_DELAY)

  ##############################################################################
  # write text to display
  def lcdText(self, line, text, fmt):

    self.__lcdByte(line, LCD_CMD)
    self.__lcdString(text,fmt)
    
  ##############################################################################
  # set back light on or off
  def lcdSetBackLight(self, state):
    GPIO.output(LCD_BACKLIGHT, state)

  ##############################################################################
  # re-initialise display (reset corruption) 
  def Init(self):
    self.__lcdInit()
    
  ##############################################################################
  # class for scrolling text in separate thread
  class Scroller(threading.Thread):
    def __init__(self, lcd, spd=0.2):
      self._stop = threading.Event()      # event to indicate to stop
      threading.Thread.__init__(self)     # initialise thread state
      self.lock = threading.Lock()        # thread lock
      self.txtLine = ""                   # complete display text
      self.displayRow = LCD_LINE_2        # display row
      self.LCD = lcd                      # create an LCD object
      self.speed = spd                    # scrolling speed
      self.daemon = True                  # start as a daemon
#      print "LCD Thread created"

    # main thread loop
    def run(self):
      displayString = ""
      count = 0
      # run until stop signal
#      print "LCD Thread running"
      while not self.stopped():
        with self.lock:
          # scroll left
          while (count < (len(self.txtLine) - LCD_WIDTH) and not self.stopped()):
            displayString = self.txtLine[count:count + LCD_WIDTH]
            self.LCD.lcdText(self.displayRow, displayString, 2)
            time.sleep(self.speed)
            count += 1

          # scroll right
          while (count > 0  and not self.stopped()):
            displayString = self.txtLine[count:count + LCD_WIDTH]
            self.LCD.lcdText(self.displayRow, displayString, 2)
            time.sleep(self.speed)
            count -= 1
            
        time.sleep(0.001)
          
#      print "LCD Thread terminated"

    # update the main text
    def updateText(self, line, text):
      self.displayRow = line
      self.txtLine = text

    # stop the thread
    def stop(self):
        self._stop.set()

    # thread stopped indicator
    def stopped(self):
        return self._stop.isSet()
