#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports

import threading

################################################################################
# test if a value is a number
def is_number(s):
  try:
    float(s)
    return True
  except ValueError:
    return False
          

threadLock = threading.Lock()
