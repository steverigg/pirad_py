#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports

# system imports
import RPi.GPIO as GPIO
import time 
import os
import re

# local imports
import PiradDB
import LCD
import PiradMenu

debug = False


################################################################################
# admin class to hold pirad functions
class PiradAdmin:

  class State:
    ##############################################################################
    # initialise state
    def __init__(self):   
      self.stopped = True            # indicate stopped state
      self.inMenu = False           # leave menu system
      
    def getStoppedState(self):
      return self.stopped
      
    def setStoppedState(self, state):
      self.stopped = state
      
    def getMenuState(self):
      return self.inMenu
      
    def setMenuState(self, state):
      self.inMenu = state

  ##############################################################################
  # initialise state
  def __init__(self, lcd, db, menu):
    self.lcdDisplay = lcd
    self.db = db
    self.menu = menu
    self.state = self.State()

  ################################################################################
  # display introductory text
  def lcdTitle(self):

    self.lcdDisplay.lcdText(LCD.LCD_LINE_1, "piRad", 2)
    self.lcdDisplay.lcdText(LCD.LCD_LINE_2, "Internet Radio", 2)
      
    time.sleep(2)

  ################################################################################
  # display introductory text
  def resetLCD(self):

    self.lcdDisplay.Init()
    self.lcdDisplay.lcdText(LCD.LCD_LINE_1, "piRad", 2)

  ################################################################################
  # load streams from DB
  def loadStreams(self):

    # check we can connect to DB and reset stream list
    if self.db.canConnect():
      os.system("mpc clear")
      urlList = []
      niceList = []
      self.menu.clearStream()
      if (self.getStreamListFromDB(urlList, niceList)):
#      if (self.db.dbGetStations(urlList, niceList)):
        for i in range(len(urlList)):
          os.system("mpc add '" + urlList[i] + "'")
          self.menu.addStream(niceList[i])
          if debug: print niceList[i] + " : " + urlList[i] + " added"
      else:
        print "Failed to get sources"
    else:
      print "Failed to open DB"

  ################################################################################
  # load streams from DB
  def getStreamListFromDB(self, urls, friendly):
      urlList = []
      niceList = []
      if (self.db.dbGetStations(urlList, niceList)):
        for i in range(len(urlList)):
          urls.append(urlList[i])
          friendly.append(niceList[i])
        return True
      else:
        urls = None
        friendly = None
        return False
    
  
  ################################################################################
  # load last played station
  def getLastStation(self):
    retVal = None
    if self.db.canConnect():
      retVal = self.db.dbGetSetting("LastStation")
      if debug: print "Last station: " + str(retVal)
    else:
      print "Failed to open DB"
    return retVal

  ################################################################################
  # save last played station
  def setLastStation(self, value):
    if self.db.canConnect():
      retVal = self.db.dbSetSetting("LastStation", value)
    else:
      print "Failed to open DB"

  ################################################################################
  # obtain local ip address from system
  def getIpAddress(self):
    retVal = ""
    # get current information
    f=os.popen("ip addr | grep wlan0 | awk '{print $2}' | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}'")
    for i in f.readlines():
      retVal += i
    f.close()
    return retVal
          
  ################################################################################
  # obtain ESSID from system
  def getEssid(self):
    retVal = ""
    # get current information
    f=os.popen("iwconfig wlan0 | grep ESSID | awk '{print $4}' | grep -o \".*\"")
    for i in f.readlines():
      retVal += i
    f.close()
    matchObj = re.match( r'(.*")(.*)(".*)', retVal)
    retVal = matchObj.group(2)
    print retVal
    return retVal
          
  ################################################################################
  # obtain MAC address from system
  def getMACAddress(self):
    retVal = ""
    # get current information
    f=os.popen("ip addr | grep link/ether | awk '{print $2}'")
    for i in f.readlines():
      retVal += i
    f.close()
    return retVal
                
  ################################################################################
  # get current stream information
  def getStreamInfo(self):
    f=os.popen("mpc current")
    station = ""
    for i in f.readlines():
      station += i
    f.close()
    return station      

  ################################################################################
  # play stream
  def play(self, idx=-1):
    if(idx==-1): idx=self.getLastStation()
    os.system("mpc play " + str(idx))    # if so, play it
    self.db.dbSetSetting("LastStation", str(idx))
    self.lcdDisplay.lcdSetBackLight(LCD.BKLT_ON)
    self.lcdDisplay.lcdText(LCD.LCD_LINE_2, "", 2)
    self.state.setStoppedState(False)            # indicate stopped state
    self.state.setMenuState(False)  # leave the menu

  ################################################################################
  # stop playing
  def stop(self):
    os.system("mpc stop")     # and stop playing, if so
    self.lcdDisplay.lcdSetBackLight(LCD.BKLT_OFF)
    self.lcdTitle()                # display original title screen
    self.state.setStoppedState(True)            # indicate stopped state
    self.state.setMenuState(False)           # leave menu system

  ################################################################################
  # reload streams
  def reload(self):
    self.stop()                         # and stop playing, if so
    self.loadStreams()             # and reload the streams
    self.lcdTitle()                # display original title screen

  ################################################################################
  # reboot system
  def reboot(self):
    self.stop()                    # and stop playing, if so
    os.system("sudo reboot")  # and reboot

  ################################################################################
  # shutdown system
  def shutdown(self):
    self.stop()                    # and stop playing, if so
    os.system("sudo halt")    # and shutdown


